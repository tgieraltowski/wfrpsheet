'use strict';

const React = require('react');
const ReactDOM = require('react-dom');
const client = require('./client');

class App extends React.Component {

	constructor(props) {
		super(props);
		this.state = {heroes: []};
	}

	componentDidMount() {
		client({method: 'GET', path: '/api/heroes'}).done(response => {
			this.setState({heroes: response.entity._embedded.heroes});
		});
	}

	render() {
		return (
			<HeroesList heroes={this.state.heroes}/>
		);
	}
}

class HeroesList extends React.Component{
	render() {
		const heroes = this.props.heroes.map(hero =>
			<Hero key={hero._links.self.href} hero={hero}/>
		);
		return (
			<table>
				<tbody>
					<tr>
						<th>Name</th>
						<th>HP</th>
					</tr>
					{heroes}
				</tbody>
			</table>
		);
	}
}

class Hero extends React.Component{
	render() {
		return (
			<tr>
				<td>{this.props.hero.name}</td>
				<td>{this.props.hero.hp}</td>
			</tr>
		);
	}
}

ReactDOM.render(
	<App />,
	document.getElementById('react')
)