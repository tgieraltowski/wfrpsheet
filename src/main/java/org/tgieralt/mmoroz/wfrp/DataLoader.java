package org.tgieralt.mmoroz.wfrp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 *
 * @author tgier
 */
@Component
public class DataLoader implements CommandLineRunner {
    
    private final HeroRepository heroRepository;

    @Autowired
    public DataLoader(HeroRepository heroRepository) {
        this.heroRepository = heroRepository;
    }

    
    @Override
    public void run(String... args) throws Exception {
        this.heroRepository.save(new Hero("Hermann", 14));
        this.heroRepository.save(new Hero("Bilbo", 12));
        this.heroRepository.save(new Hero("Brunhilda", 10));
        this.heroRepository.save(new Hero("Gimli", 18));
    }
    
}
