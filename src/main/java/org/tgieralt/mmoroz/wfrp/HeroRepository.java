package org.tgieralt.mmoroz.wfrp;

import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author tgier
 */
public interface HeroRepository extends CrudRepository<Hero, Long>{
    
}
